.. _sect_function_create_game_round:

=========================================
The function :py:func:`create_game_round`
=========================================

The section ":ref:`sect_template_modify_create_game_round`" explains the basics of modifying function :py:func:`create_game_round`. This section gives more details.


.. _sect_answer_style_options:

-------------------------------------------------------------
Answer style options ('answer_type' in ``create_game_round``)
-------------------------------------------------------------

The game developer has several possibilities for the style of answer they wish for their game. The choice is made in function ``create_game_round`` in file "logic.py".

.. note:: When scoring a game round, usually the player gets full marks for a correct answer and (for example for MCQ-type games) a player can lose some marks for an incorrect answer. However, there is also the concept of a partial mark in games. In such a case, a player may get some marks for a good attempt even if their answer is incorrect. In the descriptions below, it is mentioned for which game styles the concept of a partial mark is supported.

The full list of possibilities is:

    ``AnswerType.String``
        The answer to the game round is a string. No marks will be given
        for a partially correct answer (i.e. there is no partial mark).

    ``AnswerType.StringCaseI``
        The answer to the game round is a string. No marks will be given
        for a partially correct answer (i.e. there is no partial mark).
        The comparison is case insensitive.

    ``AnswerType.Partial``
        The answer to the game round is a string. The partial mark will
        be the length of the correct prefix. (Default.)

    ``AnswerType.PartialCaseI``
        The answer to the game round is a string. The partial mark will
        be the length of the correct prefix. The comparison is case
        insensitive.

    ``AnswerType.MCQ``
        The answer to the game round is a label for one of the multiple
        answers presented to the player (it was a multiple-choice
        question). Negative marking is used for the partial mark.

    ``AnswerType.ImgMCQ``
        The same as ``MCQ`` but allows images to be displayed instead of
        text in the Brython version of the game

    ``AnswerType.Int``
        The answer to the game round is an int. No marks will be given
        for an incorrect answer.

    ``AnswerType.NaturalMin``
        The answer to the game round is a non-negative int. The player's
        answer will be used as the partial mark. An answer greater than
        or equal to the target is deemed correct. If there is a goal,
        and that goal is achieved, the partial mark will be doubled.

    ``AnswerType.NaturalMax``
        The answer to the game round is a non-negative int. An answer
        less than or equal to the target is deemed correct. If correct,
        the target minus the player's answer will be used as the partial
        mark. If there is a goal, and that goal is achieved, the partial
        mark will be doubled.

    ``AnswerType.Bag``
        The answer to the game round is a bag (an unordered list, or,
        equivalently, a set that can contain multiple elements). The
        number of elements in the set will be used as the partial mark.
        No marks will be given for an incorrect answer. It is assumed
        that the bag contains strings (as that is what comes naturally
        from the command line) and that each string contains no spaces
        and commas.

    ``AnswerType.BagCaseI``
        As with ``Bag`` above, except the strings are compared case-insensitively.
