.. _sect_advanced_functionality_in_cl_py:

=================================================
Advanced functionality supported in :file:`cl.py`
=================================================

This section describes advanced functionality available through the :file:`cl.py` file for each game that you may need as your game becomes more sophisticated.

Before reading this section, you should be familiar with the basic functionality supported in the :file:`cl.py` file for each game. If you have followed this documentation in order, you will have read about this basic functionality. As a reminder, the relevant sections of the documentation were:

    - section ":ref:`sect_round_behaviour_ranges`",
    - section ":ref:`sect_default_round_behaviour`",
    - section ":ref:`sect_update_cl_help_custom`",
    - section ":ref:`sect_modify_construct_cl_formatted_strings`", and
    - section ":ref:`sect_modify_the_strings_in_the_game`" about the file :file:`text_constants.py`.


.. _sect_using_relative_range:

--------------------------------------------------------------
Using a ``RelativeRange`` object in ``ROUND_BEHAVIOUR_RANGES``
--------------------------------------------------------------

..
    **HIDDEN**
    <<TODO: complete this section>>

The use of a ``RelativeRange`` object gives you more control over the allowed parameters for a game round so you can avoid boilerplate code sanitising inputs. It is useful in cases where the allowed values for a particular parameter cannot be independently stated, but actually depend on the value chosen for another parameter.

..
    **HIDDEN**
    e.g.
        index=RelativeRange(ge=0, lt='length'),
        collect=RelativeRange(ge=1, le='length')
        solution=RelativeRange(ge=3, lh='colours'),
        height=RelativeRange(gt='solution', le=20)
    Four bounds (<, <=, >=, >) can be specified. The closed inequality
    <= (respectively, >=) takes precedence if both it and its open
    counterpart < (respectively, >) are specified.
    In contrast with Python's standard library `range` object, the
    closed lower bound <= (equivalent to the `start` parameter in
    Python's `range`) does not have a default value.
    Parameters
    ----------
    ge : int-like or dict key, optional, default ``None``
        The closed lower bound (>=) of the range, either something that
        maps to an int, or the name of a key in a dict whose value maps
        to an int.
    gt : int-like or dict key, optional, default ``None``
        The open lower bound (>) of the range, either something that
        maps to an int, or the name of a key in a dict whose value maps
        to an int. This will be ignored if attribute `ge` is not
        ``None``.
    lt : int-like or dict key, optional, default ``None``
        The open upper bound (<) of the range, either something that
        maps to an int, or the name of a key in a dict whose value maps
        to an int. This will be ignored if attribute `le` is not
        ``None``.
    le : int-like or dict key, optional, default ``None``
        The closed upper bound (<=) of the range, either something that
        maps to an int, or the name of a key in a dict whose value maps
        to an int.

..
    **HIDDEN**
    <<TODO: include examples from CTGames>>


.. _sect_using_relative_default:

-----------------------------------------------------------------
Using a ``RelativeDefault`` object in ``DEFAULT_ROUND_BEHAVIOUR``
-----------------------------------------------------------------

<< TODO >>

..
    **HIDDEN**
    This isn't so helpful (below). Basically, include as a string the name of the parameter who's value you wish to use.
    """A class that allows relative values to be specified.
    Instead of specifying a default value, with an `int` for example,
    a key from a `dict` can be specified, and the value associated with
    that key will become the default value. This allows one to specify
    some, as yet undecided, value because the `dict` does not need to
    have been defined before this object is created.
    Parameters
    ----------
    relative_default : hashable object
        The `dict` key whose value is used as the default value.
    """

..
    **HIDDEN**
    <<TODO: include examples from CTGames>>
