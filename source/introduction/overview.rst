.. _sect_introduction_overview:

========
Overview
========


Motivation
----------

In summer 2021, after a two-year pilot, students will sit the first Leaving Certificate Computer Science examination in Ireland.
If, in time, computer science becomes an official primary school subject, we may find it necessary to teach the subject using programming languages that are computationally simpler than languages such
as Java, Python, C, Pascal, and so on.

One possibility is to design computer games that each require a fundamental programming skill to win. The CTGames project involves designing simple, approachable, and fun computer games, that each teach a fundamental computer science-related concept or skill to primary school-age children.


Characteristics of a CTGames game
---------------------------------

Although we call them "games," the games that are developed as part of CTGames are not like regular computer games. Rather, they are gamified tutorials on aspects of computational thinking, as we explain in this section. From the game programmer's perspective, each game in CTGames should be simple enough to test aspects of a single computer science concept or skill, such as sequential composition, abstraction, stepwise refinement, divide and conquer, operation of a stack, unit testing, and so on.

Each game will involve strategy and puzzle-solving in a restricted environment governed by clear rules and without randomness (other than the random generation of game instances). In particular, these games
will not be based on quick reactions in a dynamically changing environment (e.g. Tetris, a platform
game, car racing, first-person shooter, and so on).

The games will usually consist of rounds of multiple choice questions, but can incorporate a restricted range of other interaction possibilities such as entering symbols through a virtual keyboard, dragging and dropping elements into specific areas, and so on. 


Examples of Bebras tasks
------------------------

Game developers often come up with their own game ideas, but the default option is to define the game idea based on one of the 1000+ Bebras computational thinking tasks already been designed through our `Bebras`_ international partners.

.. _fig_example_task_baggage_belt:

.. figure:: example_task_baggage_belt.png
    :width: 700
    :align: center

    Example Bebras task about a circular list

For example, the figure ":ref:`fig_example_task_baggage_belt`" is a visualisation of one such simple task. Suitcases fall onto an airport baggage belt with particular rules that ensures at most one suitcase per baggage belt section. The player wins the game by correctly determining what will be the final distribution of bags on the conveyor belt (A, B, C, or D), for one particular randomly generated input sequence of bags.

.. _fig_example_task_warehouse:

.. figure:: example_task_warehouse.png
    :width: 400
    :align: center

    Example Bebras task about concurrent programming

In another example, shown in figure ":ref:`fig_example_task_warehouse`", the player must decide what common instructions can be given to each robot such that they each pick up a different shape.


Command-line versions of CTGames games
--------------------------------------

All CTGames games start out as command-line games. In this form, the programmer develops the logic of the game and ensures its playability. The command-line inputs and outputs have strict restrictions that allows a thin graphical web app layer to be added with minimal effort. The command-line games are developed with a recent version of Python 3.


Graphical versions of CTGames games
-----------------------------------

Each CTGames game is presented to the schoolchild player in the form of a web app, played through a web browser. The graphical versions of the games should be visually appealing, but the graphical quality does not have to be very high. These games do not have to compete with commercial games on the Google Play Store, for example, for children's attention. The games will be a classroom activity set by the schoolteacher, so in that sense we have a captive audience. The graphical elements are there to keep children interested, somewhat hide the underlying computer science and mathematical elements, and ensure children can play the games intuitively with minimal instructions.

Some restrictions will exist on the nature of graphical interactions that the game programmer can implement for their game due to the nature that the web app will always be a thin layer over what is essentially a command-line game.

The web app games, written in Python, will directly re-use (without modification) the command-line versions of the games. As such, the command-line versions of a game needs to be working before web app development is practical. The Python web apps will be executed using an implementation of Python that runs in web browsers (called `Brython`_) so that the games will run on a range of platforms and browsers.

.. _Bebras: https://www.bebras.org/
.. _Brython: https://www.brython.info/
