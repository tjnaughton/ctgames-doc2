.. _chap_introduction:

.. centered:: Chapter: Introduction

=====
About
=====

CTGames is a framework for the development of very simple computational thinking games. The inspiration for the games are `Bebras`_ tasks. Bebras tasks are often presented online and often have dynamic drag and drop features, but the task is static in the sense that the question, difficulty level, and correct answer are hardcoded. With CTGames, we develop Bebras-inspired games that randomly generate different instances of the task that adapt dynamically to the ability of the player.

The conventional Bebras approach of presenting a student with completely different tasks in sequence allows one to develop new problem solving skills. Our complementary view, effected with CTGames, is that repeating different instances of the same task can allow a student to practice and refine on particular problem solving strategy.

Started in April 2016, CTGames is developed by undergraduate and masters students at Maynooth University Department of Computer Science, under the supervision of Tom Naughton. It is part of the Maynooth University `PACT`_ initiative to teach computer science/computational thinking to schoolchildren.

The project is in the process of collecting permissions from all previous students so that the CTGames code can be distributed under a free and open source licence.

.. _Bebras: https://www.bebras.org
.. _PACT: https://pact.cs.nuim.ie
