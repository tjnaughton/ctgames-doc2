.. _sect_introduction_technologies:

============
Technologies
============

This section will introduce Python, Brython, and SVG, the three technologies that a programmer will need to understand in order to programme games for CTGames.

Other technologies are used under the hood of the CTGames framework, but are not exposed to the game programmer. The teacher portal is implemented using a Flask server, Nginx web server, PostgreSQL database, each in its own Docker container. More details on the teacher portal can be found in the chapter ":ref:`Teacher portal development <chap_portal_development>`".


Python
------

`Python`_ is a high-level programming language. There are many articles, tutorials, and textbooks about Python. You might be interested in this `Wikipedia article on Python`_. There are many good textbooks available for free online. One old, but still good one is "Think Python - How to Think Like a Computer Scientist," 2nd Edition, Green Tea Press, by Allen B. Downey. It has `HTML and PDF versions <https://greenteapress.com/wp/think-python-2e/>`_ available and an `interactive online version <https://runestone.academy/runestone/books/published/thinkcspy/index.html>`_ that includes videos and allows you to execute examples of code from the book directly in the web page.

Please note, learning Python from a book takes a long time. If you are a reasonably competent programmer in some other imperative programming language (such as Java or C) then you should just jump in and start programming a CTGames game using the supplied template and code from other games as a guide. Online articles and tutorials, and websites such as `Stack Overflow <https://stackoverflow.com/questions/tagged/python?tab=Votes>`_, will be valuable aids to help you understand a topic more deeply (such as the :py:class:`range` class in Python) at the point in time that you specifically need it.


Lists of free online Python books
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The links in the list of lists below work as of December 2020. They are lists I've come across, or Google has ranked highly, rather than the best lists I could find with an in-depth search. By their nature, some links may have broken by the time you read this, or some books may not be free any more.

.. admonition:: Free online Python books

    - https://javinpaul.medium.com/best-python-books-a93d1a0d842d (October 2020)
    - https://hackr.io/blog/best-python-books-for-beginners-and-advanced-programmers (September 2020)
    - https://medium.com/javarevisited/my-favorite-books-to-learn-python-in-depth-77465633b46e (April 2020)
    - https://www.guru99.com/best-python-books.html (January 2020)
    - https://www.pythonkitchen.com/legally-free-python-books-list/ (January 2020)
    - https://realpython.com/best-python-books/ (July 2019)
    - https://www.techrepublic.com/article/learning-python-best-free-books-tutorials-and-videos/ (July 2019)
    - https://stackabuse.com/the-best-python-books-for-all-skill-levels/ (February 2018)
    - https://www.java67.com/2017/05/top-7-free-python-programming-books-pdf-online-download.html (July 2017)
    - https://codeburst.io/15-free-ebooks-to-learn-python-c299943f9f2c (March 2017)


Brython
-------

`Brython`_'s goal is to replace Javascript with Python as the scripting language for web browsers.

Useful resources to learn about Brython
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Understanding `Brython`_ is not necessary to use the CTGames framework, so checking out these resources is not necessary. However, if you want to learn about Brython, the suggested lists. As always, :abbr:`GIYF (Google is your friend)` for the most up-to-date links to resources.


SVG
---

TODO: SVG (scalable vector graphics) is...


GSAP
^^^^

TODO: GSAP is...

.. _Python: https://www.python.org/
.. _Brython: https://www.brython.info/
.. _Wikipedia article on Python: https://en.wikipedia.org/wiki/Python_(programming_language)
