.. _chap_game_improvements:

.. centered:: Chapter: Game Improvements

=================================
Introduction to game improvements
=================================

This chapter is designed to record thoughts for how to improve nearly-finished games or games that could be improved. It may be edited regularly, and may suggest multiple options, and so its contents may not yet be in an appropriate form to be opened as an issue on the CTGames git repository.

If someone does tackle an issue in this document, they should delete the relevant text from this document, open an issue on the CTGames git repository with the text, assign the issue to themselves, and then close the issue with a mention of the 8-character commit hash (e.g. 203bfd1d) in the close message.

For each game, the notes are in four categories:

- *Framework*: what needs to be done to adapt the game to the latest version of the *Framework*,

- *Logic and gameplay*: what needs to be done to improve the command-line version of the game, or gameplay (e.g. configuration file),

- *Graphics and animations*: what graphical and/or animation elements need to be improved, and

- *Code quality*: are there any missing docstrings and comments, unbounded loops during random generation of game instances, PEP8 issues, large blocks of similar-looking code that could be refactored, and so on.


====================
General improvements
====================

This section describes improvements to the framework that would have a visible effect on all games.

- Improvements to this documentation.
- Common child-friendly typeface for text in all games.
- Common scoreboard for each game.
- A progression system, consisting of achievements to obtain (e.g. a badge for finishing any 3 games), stars to obtain (different numbers or colours of stars for progression in each game), ranking (novice through to expert) based on a combination of badges and stars that leads to unlocked harder levels.


==========================================
Improvement suggestions for specific games
==========================================

The following games are listed in alphabetical order.

Animal Dominos
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Decide MCQ ans.

*Graphics and animations*

No instructions or facts.


Balloons
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

See comment in logic file "should `string` be returned here rather than `target_as_list`?"

*Graphics and animations*

Larger images on buttons. (Tom)


Birthday candles
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK

*Logic and gameplay*

OK

*Graphics and animations*

- unnecessary extra candle in early rounds which is always 0, doesn't match chart which only shows 3 candles.

Beavers on the Run
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK

*Logic and gameplay*

Construct a seq of animation frames in the logic file

*Graphics and animations*

- There should some way for the developer to control the relative positions of the underground objects in the following way: Specify whether it should be in top third, middle third, or bottom third of the underground section
- Animate the beavers to show the correct answer in the web app
- New input option. Either image selects or a new drag and drop to allow players to specify the order.

Code quality
---
Major obvious non-Pythonic issues


Bowls
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Cfg with 4 levels and 3 sublevels

*Graphics and animations*

Scale bowls according to length of tower.


Bracelet
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

- Errors are evident at higher levels (completely different colours in question and answers).
- Creation of new round should use shuffle_answers not hard-code the shuffle.

*Graphics and animations*

OK.

Bungee Jump
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Might be better for answer box to begin blank instead of with a zero so answers can be typed more easily, as it's more convenient than clicking up number by number.
Or perhaps number could start at the lowest possible option listed based on previous guesses (if we know the answer is higher than 82, it should start on 83)

*Graphics and animations*

Window image could be better cropped.

Car Transportation
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

- Creation of new round should use shuffle_answers not hard-code the shuffle.
- Sublevel category doesn't seem to work correctly.
- Incorrect answers are too dissimilar from correct answers and so answers can be calculated from the first 2 or 3 cars and so harder questions with more cars don't actually increase much in difficulty.

*Graphics and animations*

First image on 'How to play' screen could be smaller.
Only 1 image for car minutes when 2 were required on some questions.

Caterpillar
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Significant PEP8 tidying needed.

*Graphics and animations*

Group animation not working. (Tom)
Other obstacles in playing area (Dirt, Spider, Fence instead of rocks around field)


Centipede
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Broken, game does not load

*Logic and gameplay*

OK.

*Graphics and animations*

Animate shoes on centipede and shoe counter decreasing.


City Traffic
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.


Coins
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Teacher example does not use duplication of coins were as all questions do. (Low priority)

*Logic and gameplay*

Precomputed rounds or greedy algorithm. (Tom)
Sublevel percentage stuck at 0%

*Graphics and animations*

OK.


Coinsum
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Precomputed rounds. (Tom)
Longer CFG. (Tom)

*Graphics and animations*

OK.

Coloured Balls
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Options in index within question are incorrect in earlier levels (listing drops from cylinder 2, when only 0 and 1 are in the question)
Sometimes has answers which are incorrect as there are no errors in the question.

*Logic and gameplay*

Balls seem to

*Graphics and animations*

OK.


Common Pair
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

correct answer sequence could be sped up slightly.


Concurrent Warehouse
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Compute all animation paths in logic file??? (Tom)
Doesn't respond to input of answer

*Graphics and animations*

Show player's selected animation, not only correct one??? (Tom)


Count From Zero
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Scaled images on buttons. (Tom)


Crane
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.


Crane Plus
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Broken.

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.


Cross Country
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Extra possibilities to extend game:
- which terrains are identity functions?
- for given runners' starting positions, which terrains are identity fns?
- for these starting positions and ending positions, which terrains will effect that transformation?
- new terrain - water: blue overtakes one beaver
- new terrain - thistles: leading beaver moves to back of pack

*Graphics and animations*

- New player input modality (drag and drop?). (Tom)
- Convert graphics to SVG format.
- Add animation of some sort.
- Tutorial needs colour-impaired-proofing.


Daisy Chain
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.

Dinner
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

More levels could possibly be an improvement.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.


Directions
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

- There are errors in the game: sometimes two MCQ answers are correct
- Compute all animation paths in logic file??? (Tom)
- Smaller increments in game difficulty between sublevels

*Graphics and animations*

- Show player's selected animation, not only correct one??? (Tom)
- New tutorial with two examples shown (1 vehicle and 3 vehicles). Show all outcomes for each example.
- Compass large and on the right

Code quality
---
- `animations_list` needs reviewing here. It does not seem to use the conventional approach to specify an animation in a game.
- several while loops should be removed


Dream Dress
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

- Improve behaviour.py file.
- Limit ranges of sublevelbehaviour parameters in cl.py file.

*Graphics and animations*

OK.


Dungeon Escape
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.


Egg Colouring
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Remove whitespace from svg images on disk.
Overlapping images on 'How to play' screen, difficult to understand.


Fireworks
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Highest Level percentage doesn't increase to 100 on game completion 

*Graphics and animations*

Showing both versions of answer (words and images) could make it easier to see why the answer is wrong 


Flowers
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Pre computed levels. (Tom)

*Graphics and animations*

OK.


Flowers Plus
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

- Add more levels.
- Precomputed levels. (Tom)
- 'How to play' mentions possibility of multiple answers being correct, but this never seems to be the case

*Graphics and animations*

Extra easier tutorial.


Frog Jump
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Ponds should have non-regular shapes.
Animations for jumps would be a good addition.


Frog Path
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

The game seems to be broken: in the webapp the correct path always seems to be some bogus string such as 111111111. However, the command line seems to be working. Possibly this is a Brython bug.

*Graphics and animations*

Sometimes there is a can't find image error: GEThttp://127.0.0.1:8080/ctgames/frogpath/resources/images/.svg
Ponds should have non-regular shapes.


Fruit
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Low priority: Animate Monsters Head when correct answer (replace static image with SNAP SVG?)


Funny Windows
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Some Levels dont give correct answers

*Graphics and animations*

OK.


Horse
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Broken! game does not load

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.


Infinite Ice Cream
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

The correct answer is suggested as "0" or "1" rather than something more easily understandable.


Jersey Numbers
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Should beavers be different heights as in Bebras task?


Jigsaw
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Graphic of correct answer if wrong


Jigsaw Plus
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Longer cfg.

*Graphics and animations*

Centre upper list.


Just Reverse
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.


Kangaroo
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Kangaroo dissapears from screen slightly when jumping.
Images on help screen could be smaller.


Loading Lisas
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Scoring using "NaturalMax" does not seem appropriate.
Precomputed rounds. (Tom)
Provide path variable in precomputed rounds. (Tom)
Asks for typed answer when there is only drag and drop 

*Graphics and animations*

Boats for drag and drop. (Tom)
Tutorial
Better response to incorrect answer
Instructions/Written question are missing 


Lollipops
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.


Luggage
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

- Can sometimes be too easy to find answer by just looking for the single difference in each answer

*Graphics and animations*

- Group animation does not work. (Tom)
- Add rules to tutorial.
- Second tutorial, showing what happens with a filled position.
- Remove brown triangle on conveyor.
- Images could be smaller on 'How to Play' screen 


Median Height
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Description different from actual game. Doesn't require sorting.

*Logic and gameplay*

Answers are sometimes incorrect, or are answers that weren't even options.

*Graphics and animations*

Missing images.
Numbers sometimes overlap image.
Broken image in 'How to play' screen.


Min Max Tree
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.

Molecules
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Very few levels.

*Graphics and animations*

OK.


Most Frequent
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.

Necklace
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.

My New Game
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

No Content.

*Logic and gameplay*

OK.

*Graphics and animations*

Ok.

Ordered
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Question template needs to be changed when the question is changes to sides in ascending order, giving colour order here is uneccessary and confusing.
No need for image of colour order and worder color order
Answers need to be labelled: (A),(B),etc

*Logic and gameplay*

Two correct answers showed up, only one actually correct


*Graphics and animations*

OK.


Ordered List
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

behaviour.py file needs reviewing (meaningful increment when changing level)

*Graphics and animations*

OK.


Palindromes
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Different game modes, eg childrens names, cities etc.

*Graphics and animations*

OK.


Parking Lot
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.


Party guests
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

No science fact 

*Logic and gameplay*

Order should be included within each question instead of just tutorial, unintuitive

*Graphics and animations*

OK.


Password
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

When the player gets it wrong, should we explain why A) is the correct answer,
i.e. square = Q, triangle = W, ...


Pile Of Blocks
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

- Shorter descriptions for the SublevelBehaviour elements
- Split game into two: one regular and the second using "blocks with replacement"?

*Logic and gameplay*

Merge previous version of Pile of Blocks with new version (e.g. text_constants.py)

*Graphics and animations*

Label target pile with 'Target' and move more to the right.
Colour selected index yellow at each point of animation. (similar to most frequent)


Pollen
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.


Presents
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Tutorial displays over actual game

*Logic and gameplay*

- Fix ranges of sublevelbehaviour parameters in cl.py file, in particular to cop with values in cfg file.

*Graphics and animations*

- Correct answer not displaying correctly when player answers incorrectly.

Code quality
---
Code needs to be refactored (docstrings for functions and SublevelBehaviour, comments, remove a while loop)


Reflections
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Bug: when red square goes into starting position it should not bounce out.
Bug: when beamsplitters are removed, game crashes regularly (game relies on beam splitters to solve levels) 
cfg file should have 20 levels
add rounds_needed

*Graphics and animations*

Two example games in tutorial, showing start and end states
Letters slig
htly too low on squares

Rollercoaster
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Science fact has to be added.
No tutorial

*Logic and gameplay*

When children are both leaving and joining the que, directions contain mistakes.

*Graphics and animations*

OK.


Slicing
^^^^^^^^^^^^^^^^^^^^^^^^Precomputed rounds. (Tom)

*Framework*

Bug: In a custom round in the web app, the drop down box for "label" does not show all of the possibilties.
Science fact at start must be added

*Logic and gameplay*

Longer cfg (Tom, I've added rounds needed in lieu of this because there isn't much scope for different values)

*Graphics and animations*

Second tutorial with more than one slice.
Tutorials image needs to be made smaller

*Graphics and animations*

OK.

Selfish Squirrels
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

Science fact has to be added.

*Logic and gameplay*

OK.

*Graphics and animations*

Need to change the game logo: it shows beavers, not squirrels.

Slice
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.


Soup
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.

Spherical Robot
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

Robots speed should be sped up for later rouds.


Throw the Dice
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

OK.


Toothbrush
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Precomputed Rounds.
Doesn't respond to input.

*Graphics and animations*

OK.


Trading
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Ok.

*Graphics and animations*

Add image of cupcakes to question instead of text 'cupcake'
Add each state of inventory to tutorial.
Simplify tutorial


Tunnel
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Text for players needs improvement everywhere.

*Graphics and animations*

- If sticking with the existing input option (select boxes) then at the start of each round, the default selected options in each box should be the beaver at that position
- New input option (better than select boxes)
- 'How to play' rules are somewhat difficult to understand, seemingly has typos in answer explanation
- On bottom image of 'How to play' screen, Beaver B and Beaver C have a square white background while others don't


Watering
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

Precomputed rounds. (Tom)

*Graphics and animations*

Tutorial.
Same flower dead alive.
Occasional incorrect computation of answer.


You Won't Find It
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

More words: _get_random_word() in you won't find it (10-20 per number)
Add length of word to sublevel behaviour, change list of words to dict of word length: list of words.
Improve cfg to get harder more slowly and to have easier levels.
Allow teacher to suggest a word.

*Graphics and animations*

'How to play' screen images could be centered, smaller, and have some kind of border


Zebra tunnel
^^^^^^^^^^^^^^^^^^^^^^^^

*Framework*

OK.

*Logic and gameplay*

OK.

*Graphics and animations*

OK.

