.. _sect_checklist_for_web_app_completion:

================================
Checklist for web app completion
================================

Please see the companion section ":ref:`sect_checklist_for_game_completion`" for the checklist for command-line CTGames games. Both checklists need to be consulted when finalising a CTGames web app.


-----------------
Functional issues
-----------------

Web app versions of games

    * The web app has a good quality tutorial with two examples: a very easy one and a medium difficulty one.
    * **Throughout the running of the web app, no exceptions should be raised in the web browser console window.**
    * In the web app, if a player gets the answer wrong, there appears an appropriate message to explain why it was wrong and what was the correct answer.


--------------------
Documentation issues
--------------------

Web app versions of games

    * The web app "Custom" screen for teachers has reasonable descriptions for the parameters (edit "cl.py").
    * The web app "About" screen (accessed through the teachers' menu) has all sections completed.
    * Each web app graphic has a licence image with the name "IMAGE_NAME - usage terms .png" for each graphical file "IMAGE_NAME" that was used in the web app. If you created the graphics from scratch, include a text file saying that and stating that you release the graphics under a CC0 licence or other appropriate licence.


-----------
Code issues
-----------

Web app versions of games

    * In "webapp/__init__.py", the name ``__date__`` should specify the approximate date the file was last edited.
    * In "webapp/__init__.py", the name ``__copyright__`` should match the value of ``__copyright__`` in "logic.py".
    * In "webapp/__init__.py", the name ``__credits__`` should have your full name preceding Thomas J. Naughton's name, e.g. ``['Josephine A. Bloggs', 'Thomas J. Naughton']``.
    * You have worked to eliminate as many "warnings" or "code style analysis" errors (yellow triangles in PyCharm) as possible.
    * There are no unexplained "magic numbers" in the code -- all values that might need to be tweaked are constants with names in ALL_CAPITALS near the top of the file or at the top of the function in which they are used.
    * All constants have a """docstring""" immediately below them describing what the constant is for.
