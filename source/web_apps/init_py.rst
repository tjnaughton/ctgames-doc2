===================================
The file :file:`webapp/__init__.py`
===================================

This section explains the purpose of each web app function recognised by the CTGames framework. Each function is optional; the web app will run without each one, but the functions are used to make the web app more engaging for children. Each function can also be used in different ways, as explained below.


-----------------------------------------------
The function :py:func:`format_problem_instance`
-----------------------------------------------

(found in :file:`webapp/__init__.py`)

.. seealso:: This function returns a namedtuple :py:class:`TextImageSeq` object. Read about this class here <TODO>.

This function is used to make the...

Examples
--------

-----------------------------------------------
The function :py:func:`update_problem_instance`
-----------------------------------------------

(found in :file:`webapp/__init__.py`)

This...


-------------------------------------------------
The function :py:func:`format_player_answer_area`
-------------------------------------------------

(found in :file:`webapp/__init__.py`)

This...


---------------------------------------------
The function :py:func:`format_correct_answer`
---------------------------------------------

(found in :file:`webapp/__init__.py`)

This...
