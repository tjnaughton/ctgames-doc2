=======================================
The namedtuple :py:class:`TextImageSeq`
=======================================

This section explains the namedtuple :py:class:`TextImageSeq` that is returned from functions :py:func:`format_problem_instance`` and :py:func:`format_correct_answer`.


--------
Examples
--------
