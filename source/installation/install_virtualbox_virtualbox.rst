.. _sect_install_virtualbox_virtualbox:

---------------------
Installing VirtualBox
---------------------

Your supervisor will email you a link to download a `virtual appliance <https://en.wikipedia.org/wiki/Virtual_appliance>`_. This is a 20 GB pre-configured virtual machine image that VirtualBox can run as a virtual computer. The virtual appliance has Ubuntu 20.04.3 LTS, a minimal installation of the Anaconda Python distribution (Miniconda) with Python 3.9.5, docker (with containers for a Flask WSGI web server, an nginx web server, and a PostgreSQL database server), the PyCharm Python :abbr:`IDE (integrated development environment)` pre-configured for the coding style adopted by CTGames, and a cloned copy of the CTGames remote git repository with a pre-installed deployment key that allows the latest changes to be pulled.

.. note:: The pre-installed deployment key will allow you to pull the latest changes from the git repository but will not allow you to push your modifications. For that, you'll have to configure your own SSH key, as described in section :ref:`sect_install_virtualbox_configuring_gitlab`.

The virtual appliance is shipped as three separate files:

    - An XML settings file describing the hardware for the virtual machine ("CTGamesYYYYMM.vbox")

    - The zipped contents of the virtual hard disk ("CTGamesYYYYMM.zip")

    - A digest file ("CTGamesYYYYMM_zip_digests.txt")

All three filenames should have the same ``YYYYMM`` code, e.g. ``202110``. Check that the zipped virtual hard disk has been downloaded correctly with one of the digests provided in the "CTGamesYYYYMM_zip_digests.txt" file. It might take a couple of minutes to check the digest, so be patient. Then unzip this zip file.

.. tip:: In Linux, for an automated digest check run
    ::

        md5sum --check CTGamesYYYYMM_zip_digests.txt 2>&1 | grep OK
        sha256sum --check CTGamesYYYYMM_zip_digests.txt 2>&1 | grep OK

    Each should return ``CTGamesYYYYMM.zip: OK`` if the file is not corrupted.

    In Windows, run
    ::

        CertUtil -hashfile CTGamesYYYYMM.zip MD5
        CertUtil -hashfile CTGamesYYYYMM.zip SHA256

    and manually compare the two digests that are returned with those in the digest file "CTGamesYYYYMM_zip_digests.txt".
    
    In both Linux and Windows, you should run these commands from the directory containing the file "CTGamesYYYYMM.zip", and replace ``YYYYMM`` in each command with the appropriate numerals.

.. tip:: Don't delete the zip file if you can spare the space. If your virtual hard disk ever gets corrupted you can get back to the state of all of your files at the time you last performed ``git push`` by doing the following: unzip the zip file again and run the three commands ``sudo apt update``, ``sudo apt upgrade``, and ``git pull``.

Go to the VirtualBox virtual machines (VMs) folder in the your home directory. For Windows, this is usually "C:\Users\username\VirtualBox VMs\", for Linux this is usually "/home/username/VirtualBox VMs/", and for macOS this is usually "/Users/username/VirtualBox VMs/", where *username* is your username.

Create a new directory in the "VirtualBox VMs" directory called "CTGamesYYYYMM" (where ``YYYYMM`` is the code used in the filenames, e.g. ``202110``) and copy all of the virtual appliance files there.

Create a new directory anywhere on your computer called "virtualbox_shared" and remember where it is. You can use this directory to share files between your host OS and the Ubuntu guest OS.

Point a browser to `<https://www.virtualbox.org/wiki/Download_Old_Builds_6_1>`_

Download VirtualBox 6.1.26 (released July 28 2021) for your chosen operating system. It may work on later versions of VirtualBox, but this has not been tested. To speed you up, here are direct links for the following operating systems:

    - Windows `<https://download.virtualbox.org/virtualbox/6.1.26/VirtualBox-6.1.26-145957-Win.exe>`_

    - OS X `<https://download.virtualbox.org/virtualbox/6.1.26/VirtualBox-6.1.26-145957-OSX.dmg>`_

    - Ubuntu 20.04 `<https://download.virtualbox.org/virtualbox/6.1.26/virtualbox-6.1_6.1.26-145957~Ubuntu~eoan_amd64.deb>`_

Install VirtualBox. Accept each of the default options during install.
