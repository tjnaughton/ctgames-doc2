.. _sect_install_options:

================================
Installation options for CTGames
================================

In order to develop games for CTGames, you need to be a Maynooth University Department of Computer Science student with an account on the local Gitlab instance `<https://gitlab.cs.nuim.ie/>`_, and you need to be added as a developer on the project `CTGames project <https://gitlab.cs.nuim.ie/ctgames/ctgames>`_.

This chapter describes the three options available to develop games for CTGames, and then goes into detail on each. The three options for the developer have different trades-off between functionality, performance, and installation effort.


.. _sect_install_options_zero:

----------------------
Option 1: Zero-install
----------------------

The zero-install option uses a web browser for all development steps. This option is explained in section ":ref:`sect_install_zero`".

Advantages:

    - you can be up and running very quickly
    - works from any device (even Android/iOS mobile devices with only 2GB of memory)

Disadvantages:

    - you cannot use this approach to try the command-line versions of games, or develop your own
    - any changes made to the git repository can take 10 minutes before they are visible in the deployed version, so it's not practical as a development environment, but can be useful for minor edits to an existing game
    - if you don't remember to stage your commits using the appropriate provided GitLab web-based tools, this approach can make the git history look quite noisy with many inconsequential tiny commits (that are not convenient to squash into a single commit since they have already been pushed to the remote repository)


------------------------------------
Option 2: VirtualBox virtual machine
------------------------------------

This option involves using VirtualBox to run a virtual machine that has the full development environment already installed and the git repository already cloned. The option is explained in section ":ref:`sect_install_virtualbox`".

Advantages:

    - you can be up and running reasonably quickly (15 minutes to 1 hour to download the virtual appliance, depending on your internet connection, plus 15 minutes following the instructions)
    - works on Windows, macOS, and GNU Linux (but not mobile devices)
    - no need to take any risks changing how your computer boots up or buying a separate hard disk drive

Disadvantages:

    - requires a little over 20 GB of disk space and a computer with at least 4 GB of memory (8 GB memory recommended because of the virtual machine overhead)
    - the performance of your :abbr:`IDE (integrated development environment)` and web browser might be sluggish if your your PC/laptop is old
    - although extremely reliable, VirtualBox is less reliable than real hardware, so your virtual hard disk may become corrupted (frequent pushes to the remote git repository is the best insurance against the virtual appliance becoming inaccessible for any reason)

-----------------------------------
Option 3: Directly on your computer
-----------------------------------

This option involves installing all parts of the development environment (Python, git, docker, and so on) and cloning the git repository directly onto your computer. The option is explained in section ":ref:`sect_install_direct`".

Advantages:

    - if you have GNU Linux installed already, this is the best option
    - you get maximum performance from your hardware and memory

Disadvantages:

    - you have to install Ubuntu onto your PC/laptop, which comes with the risk that you might accidentally delete your existing operating system if you don't pay attention (I would recommend buying a separate hard disk drive for Ubuntu if possible)
    - requires a little over 20 GB of disk space (30 GB recommended if you wish to install other apps) and a computer with 4 GB of memory (less than needed for the virtual machine option)
    - takes about 45 minutes to 90 minutes to install everything, depending on your internet connection and familiarity with the command line

--------------------------------------------
Option 2a: Custom VirtualBox virtual machine
--------------------------------------------

There is of course a do-it-yourself version of "Option 2" which involves setting up your own VirtualBox appliance with Linux and install the CTGames development environment yourself using the "Option 3" steps. This option might be taken by someone who wants to develop in a virtual environment but does wish to use the installation of Ubuntu in the VirtualBox appliance supplied by their project supervisor. However, a more likely use of these instructions is for the project maintainer to set up the VirtualBox appliance used for "Option 2". As such, the steps are given in short form as it is assumed only someone familiar with CTGames would undertake such an installation.

The steps, in short form, are as follows.

VirtualBox
----------

- Install the VirtualBox program as described in Option 2
- Click "New"
- Name: CTGames202110
- Type: Linux
- Version: Ubuntu 64 bit
- Memory: 3072 MB
- Hard disk: Create a virtual hard disk now
- File size: 20 GB
- Hard disk file type: VDI
- Storage: Dynamically allocated
- Settings: General: Advanced: Shared clipboard: Bidirectional
- Settings: System: Processor: Processor(s): set to maximum of green values
- Settings: System: Processor: Execution Cap: 100%
- Settings: Display: Screen: Video memory: set to maximum allowed
- Settings: Shared Folders: add your Desktop as a shared folder, select Auto-mount
- Start the machine and select the file ubuntu-20.04.3-desktop-amd64.iso

Ubuntu
------

- Choose "Install Ubuntu"
- Keyboard: Irish
- Choose "Normal installation"
- Select "Download updates..."
- Select "Install third-party software..."
- Choose "Erase disk and install Ubuntu"
- Choose "Dublin"
- Name: Dev
- Choose a password that will be circulated to students
- Software & Updates: Download from: Main server
- Software & Updates: Other Software: Canonical Partners
- Software & Updates: Updates: All updates, Every two weeks, Display immediately, Display every two weeks, Never
- Software Updater: run and update all
- ``sudo apt install gcc make perl``
- Devices: Insert guest additions
- Power off and restart

Desktop
-------

- Eject guest additions CD and leave only file manager, Firefox, Text editor, and Terminal in the Dock
- Settings: Appearance: Icon size: 20
- Settings: Region: Language: English (UK)
- Settings: Power: Blank Screen: Never
- Settings: Power: Automatic Suspend: Off
- ``sudo apt update``
- ``sudo apt upgrade``
- ``sudo apt install ubuntu-restricted-extras``
- ``sudo apt install meld``
- ``sudo apt install gnome-tweak-tool``
- Tweaks: Extensions: Desktop Icons: Cog: unselect Personal Folder and Rubbish Bin
- Tweaks: Top Bar: Battery Percentage, Weekday, Week Numbers
- Open Nautilus and press **Ctrl**-**h**
- ``cd ~/Templates`` and ``touch "New text file.txt"``

GitLab
------

- ``ssh-keygen -t ed25519 -C "CTGames VirtualBox YYYYMM"``
- Enter filename ``/home/dev/.ssh/gitlab_cs_key``
- No password
- ``cat /home/dev/.ssh/id_ed25519.pub``
- Go to https://gitlab.cs.nuim.ie/ctgames/ctgames/settings/repository
- Create new Deploy Key: Title: CTGames VirtualBox YYYYMM
- Paste the contents of the .pub file
- Choose: no write access (pull only)
- ``ssh -T git@gitlab.cs.nuim.ie``

Git repository, Python, and teacher portal
------------------------------------------

- ``sudo apt install git``
- Follow the steps in section ":ref:`sect_install_direct_git_repo_and_python`"
- Follow the steps in section ":ref:`sect_install_direct_portal`"
- Put the teacher name/email/password and class member PINs in a file on the desktop called "Player PINs.txt"

Software development environment
--------------------------------

- Follow the steps in section ":ref:`sect_install_direct_development_env`"
- ``cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/``
- ``python setup.py``

Deployment
----------

- Shut down the virtual machine.
- Zip the virtual disk image file "CTGamesYYYYMM.vdi" as "CTGamesYYYYMM.zip"
- ``md5sum CTGamesYYYYMM.zip >> CTGamesYYYYMM_zip_digests.txt``
- ``sha256sum CTGamesYYYYMM.zip >> CTGamesYYYYMM_zip_digests.txt``
- Send "CTGamesYYYYMM.zip", "CTGamesYYYYMM.vbox", and "CTGamesYYYYMM_zip_digests.txt" to students
