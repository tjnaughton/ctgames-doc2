.. _sect_install_direct_development_env:

================================
Software development environment
================================

Unless you have a strong preference for another :abbr:`IDE (integrated development environment)` that you are sufficiently expert with to configure yourself, it is recommended that we all use the same IDE. Currently, PyCharm is recommended for CTGames development because of its ability to configure file watchers and support for Conda environments.


-----------------------------------------------------
Install developer-related packages in the environment
-----------------------------------------------------


a. Automatic code formatting

    A tool for automatic code formatting ensures there is a common coding style throughout the CTGames codebase. It is designed for programming teams where not everyone is familiar with the standard style guide for Python. Open a terminal in any directory and enter
    ::

        cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/resources/blackpp_vb/
        conda activate ctgames
        pip install --editable .


---------------
Install PyCharm
---------------


a. Install PyCharm

    Install PyCharm by opening a terminal in any directory (:kbd:`Ctrl-Alt-t`) and entering
    ::

        sudo snap install pycharm-community --channel=2020.2/stable --classic

    ..
        **HIDDEN**
        This is where I got the specific version info: https://snapcraft.io/pycharm-community
        This is where I got the instructions for : https://docs.anaconda.com/anaconda/user-guide/tasks/pycharm
        This gives the latest version every time: sudo snap install pycharm-community --classic
        The professional version is available to students here: https://www.jetbrains.com/community/education/#students


    Start PyCharm by entering
    ::

        pycharm-community &

    This will bring you to the "Welcome to PyCharm" screen if you are starting PyCharm for the very first time. (Also if you are starting PyCharm for the first time you may be asked about settings and plugins; just click the button to accept all defaults.)

    From the "Welcome to PyCharm" screen we will perform a number of steps detailed below.

    .. note:: If you have used PyCharm before and have some open files, just click :guilabel:`File` -> :guilabel:`Close Project` for each open project to get back to the "Welcome to PyCharm" screen.

    .. note:: Depending on your graphical environment, you may be able to create a shortcut for PyCharm on your Launcher/Desktop [for example, in Ubuntu you can find its icon on the Launcher on the left-hand-side, right-click on the icon, and select :guilabel:`Add to Favorites` (sic)].


b. Import settings

    - From the "Welcome to PyCharm" screen click :guilabel:`Configure` -> :guilabel:`Import settings`
    - Select the file :file:`/home/username/gitlab.cs.nuim.ie/ctgames/ctgames/PyCharmCE2020.2_settings_archived.zip` (where :guilabel:`username` is your own username)
    - In the box that pops up labeled "Select Components to Import", click :guilabel:`Select All` and :guilabel:`OK` 
    - Click :guilabel:`Restart` when asked


c. Install plugins

    - From the "Welcome to PyCharm" screen, click :guilabel:`Configure` -> :guilabel:`Plugins` and install the "File Watchers" plugin
    - Click :guilabel:`OK`



-----------------
Configure PyCharm
-----------------


a. Open the CTGames project

    - From the "Welcome to PyCharm" screen, click :guilabel:`Open` and in the directory listing click the directory :file:`/home/username/gitlab.cs.nuim.ie/ctgames/ctgames` where :file:`username` is your own username
    - Click :guilabel:`OK` and wait; grab a cup of coffee as it may take more than 5 minutes for Pycharm to parse the CTGames project (note the progress bar at the bottom of the Pycharm app showing background tasks indexing the project)


b. Associate the project with the `ctgames` Conda environment

    - From within PyCharm select :guilabel:`File` -> :guilabel:`Settings...` to open the "Settings" window

    - Click :guilabel:`Project: ctgames` -> :guilabel:`Python Interpreter`

    - In the dropdown box labelled "Project Interpreter", select the option :guilabel:`Python 3.8 (ctgames)`

        .. note:: Only if there is no such option in the dropdown box, carry out the following steps.

            #. Click the gear icon on the extreme right-hand side and select :guilabel:`Add...` to open the "Add Python Interpreter" dialog box

            #. In the left-hand pane of the "Add Python Interpreter" dialog box, select :guilabel:`Conda Environment`

            #. In the right-hand pane select the :guilabel:`Existing environment` radio button

            #. For the text box labelled "Interpreter", click the :guilabel:`...` button, select the file :file:`/home/username/miniconda3/envs/ctgames/bin/python`, where :file:`username` is your own username (double click on this file, or click :guilabel:`OK` to return to the "Add Python Interpreter" dialog box)

            #. The text box labelled "Conda executable" should be automatically populated. If not, set it to :file:`/home/username/miniconda3/bin/conda`, where :guilabel:`username` is your own username

            #. Check the check box :guilabel:`Make available to all projects`

            #. Click :guilabel:`OK` in the "Add Python Interpreter" dialog box to return to the "Settings" window

    - In the "Settings" window, click :guilabel:`Apply` to complete the task


    ..
        **HIDDEN**
        These were the previous instructions:
        Point Firefox to
            `<https://docs.anaconda.com/anaconda/user-guide/tasks/pycharm>`_
        From the starting point of the PyCharm welcome screen, follow steps 2 through 6 of the instructions on the webpage for ``Configuring a conda environment in PyCharm``. [Note, step 3 says to select ``Preferences``, but the menu item may instead be called ``Settings``.]
        Skip step 7.
        For step 8...


c. Integrate tools with PyCharm

    .. note:: The instructions that follow will not work unless PyCharm has completed its background tasks of indexing the project (mentioned a few lines above, a visible progress bar at the bottom of the Pycharm app will indicate of indexing is still ongoing).

    Keeping the "Settings" window open (if you've already closed this window, no problem, just make sure the CTGames project is open and click :guilabel:`File` -> :guilabel:`Settings...`)

    - Select :guilabel:`Tools` -> :guilabel:`File Watchers`

    - On the extreme right-hand side click the button that has an arrow like :guilabel:`↙` (the button with the tooltip "Import")

    ..
        **HIDDEN**
        One source for these kinds of arrow symbols is https://www.piliapp.com/symbol/

    - In the "Choose a File with Watchers" dialog box, double click on the file :file:`/home/username/gitlab.cs.nuim.ie/ctgames/ctgames/pycharm_watchers.xml` (where :file:`username` is your own username), or alternatively click it once and click :guilabel:`OK`

    - Click :guilabel:`OK` in the "Settings" window to close it

    .. note:: Once in a while when PyCharm is open it will pop up a message asking you to update the File Watchers plugin. If you update, it may delete your current file watchers. If this happens, just repeat the above step.

d. Games you create will appear in :file:`CTGames/ctgames` in the project directory structure visible in PyCharm.

    Firefox is the recommended browser to run CTGames web app games, but if you encounter any strange behaviour, Chromium and Chrome are also there to check if the strange behaviour is due to CTGames or one particular browser.

    Bookmark the chapter ":ref:`Game development <chap_game_development>`" to read later how to run other students' games and how to create your own.


--------------------------------------
Install a RST editor for documentation
--------------------------------------

.. note:: This is not required unless it has been agreed that you will contribute to the CTGames documentation.

The documentation for CTGames is written using a markup called reStructuredText (RST). The RST editors I have tried are

.. _PyCharm: https://www.jetbrains.com/help/pycharm/restructured-text.html
.. _Formiko: https://github.com/ondratu/formiko
.. _ReText: https://github.com/retext-project/retext

:PyCharm_:
    PyCharm has built-in RST support with a live preview (useful when new to RST), and spell checking, however (as of 2020) it has trouble with the kind of link used in the CTGames documentation where sections titles can have style formatting (e.g. when they contain function names). It would be nice if it worked because then we could use the same editor for code and documentation.

:Formiko_:
    Formiko has spell checking, live preview, but (as of 2020) like PyCharm, does not handle links with formatting. It automatically synchronises scrolling in the preview pane. It is quite stable. It seems to be in `maintenance mode <https://github.com/ondratu/formiko>`_ rather than active development.

:ReText_:
    ReText has spell checking and live preview. Unlike Formiko, it does not automatically synchronise scrolling in the preview pane, however it does stay where you put it and so is workable. In version 7.1.0, it has an annoying habit of occasionally not letting me save my document -- I have to close the edited document to effect a file save. It doesn't understand common text style formatting roles such as \:seealso\:, \:file\:, \:guilabel\:, and so on. However, it renders documents more closely to the way Sphinx (the processor used by `Read the Docs <https://readthedocs.org/>`_) does than the others. It is under `active development <https://github.com/retext-project/retext>`_.


I propose to use ReText. To install it, open a terminal in any directory and enter
::

    sudo apt update
    sudo apt upgrade
    sudo apt install retext

Basic configuration steps include
    - :guilabel:`Edit` -> :guilabel:`Spell check` -> :guilabel:`Enable`
    - :guilabel:`Edit` -> :guilabel:`Spell check` -> :guilabel:`Set locale` and enter "en_IE" (without quotes)


..
    **HIDDEN**
    Previous instructions, assuming you have installed Miniconda as described in a previous section (and which automatically installs pip3):
    pip3 install pyenchant
    pip3 install PyQtWebEngine
    pip3 install ReText --user

The ReText editor can be run by finding the appropriate icon, or by associating all RST files (files with filenames ending with :file:`.rst`) with this program.

..
    **HIDDEN**
    To install Formiko, open a terminal in any directory and enter
    ::

        sudo apt install flatpak
        flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        flatpak install flathub cz.zeropage.Formiko

    Replace these particular lines in the file "/home/<username>/.var/app/cz.zeropage.Formiko/config/formiko.ini"
    ::

        period_save = True
        spaces_instead_of_tabs = False
        tab_width = 2
        current_line = False

    with
    ::

        period_save = False
        spaces_instead_of_tabs = True
        tab_width = 4
        current_line = True

    Then to run the Formiko editor, enter
    ::

        cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/
        ./rsteditor

    Ignore the error 'Failed to load module "gtk-vector-screenshot"'.
