.. _sect_install_direct_git_repo_and_python:

=========================
Git repository and Python
=========================


-----------------
Related Git tools
-----------------

#. Install gitk and git-gui

    The graphical tools gitk and git-gui are part of a standard full Git installation. In a terminal from any directory, enter
    ::

        sudo apt install gitk git-gui

#. Install file comparison tools

    In a terminal from any directory, enter
    ::

        sudo apt install meld

    In a terminal from any directory, enter
    ::

        sudo apt install nautilus-compare

    If this gives errors (e.g. "E: Unable to locate package nautilus-compare" on Ubuntu 20.04) then try instead
    ::

        sudo add-apt-repository ppa:boamaod/nautilus-compare
        sudo apt-get update
        sudo apt install nautilus-compare

    Now, restart Nautilus (the default file manager with Ubuntu) with
    ::

        nautilus -q

    and you should see the two context menu items "Compare Later" and "Compare to..." when you right-click on a file in Nautilus. These context menu items allow one to conveniently pick out two text files for a graphical comparison of their text differences.


--------------------------
Cloning the Git repository
--------------------------

#. Clone the existing git repository

    These instructions will clone the git repository into a well-known location in your home directory. If you choose a different directory, appropriately change the directory given in the rest of documentation. In a terminal from any directory, enter
    ::

        cd ~
        mkdir gitlab.cs.nuim.ie
        cd gitlab.cs.nuim.ie
        mkdir ctgames
        cd ctgames
        git clone git@gitlab.cs.nuim.ie:ctgames/ctgames.git
        cd ctgames
        git status

    The output from the last command should read something like "On branch master Your branch is up to date with 'origin/master'. nothing to commit, working tree clean."

#. View recent commits to the git repository

    This gives you an idea of the activity in the git repository. All team members code changes are visible. In the terminal, from any directory in the cloned git repository, enter
    ::

        gitk --all


----------------------------------------------------------------------
Install Miniconda (a mini version of the Anaconda Python distribution)
----------------------------------------------------------------------

1. Download Miniconda

    Point Firefox to

        `<https://docs.conda.io/en/latest/miniconda.html#linux-installers>`_

    and download "Python 3.9 Miniconda3 Linux 64-bit" into your "Downloads" directory. Notice the SHA256 hash of this download is provided next to the download link.

    Wait for the file to download fully. Press the key combination **Ctrl**-**Alt**-**t** to open a *new* terminal, and change to the "Downloads" directory by entering
    ::

        cd Downloads
        ls

    In the terminal, calculate the SHA256 hash of the downloaded file by typing the following (without pressing **Enter**)
    ::

        sha256sum Min

    Press the **Tab** key. If you downloaded the file correctly, the command will autocomplete to something like "``sha256sum Miniconda3-py39_4.10.3-Linux-x86_64.sh``". Press **Enter** to calculate the SHA256 hash. If this SHA256 hash matches the hash on the webpage (checking the first five and last five characters is enough), then the download completed successfully, otherwise please download again.

2. Install Miniconda

    In the same terminal, type the following (without pressing **Enter**)
    ::

        bash Min

    Press the **Tab** key. If you downloaded the file correctly, the command will autocomplete to something like ``bash Miniconda3-py39_4.10.3-Linux-x86_64.sh``. Press **Enter**.

    Press **Enter** when prompted to view the licence terms. The **Space** key can be used to scroll through the licence terms. Accept the licence terms, when asked, by typing ``yes``.

    When asked about a location, press **Enter** to accept the default location. Wait for it to install.

    If asked do you want to initialise Miniconda, type ``yes``.

3. Test Miniconda

    After installation, close the terminal (by entering ``exit``), reopen a new terminal (e.g. **Ctrl**-**Alt**-**t**), and test your installation by entering
    ::

        conda list

    You should see a list of packages installed, including ``conda`` and ``python``. Ensure Anaconda Python has been installed by default, by entering
    ::

        python

    If you see something like "``Python 3.9.5 (default, Jun  4 2021, 12:28:51) [GCC 7.5.0] :: Anaconda, Inc. on linux``" then Miniconda was installed correctly. Press **Ctrl**-**d** to exit Python. Keep your terminal open.
