.. _sect_install_direct:

================================================
Installation option 3: Directly on your computer
================================================

This section assumes that you're using Ubuntu, version 20.04 or later, installed directly on your computer (this section is not relevant if you plan to use a VirtualBox virtual machine instead). The VirtualBox option has simpler instructions, and is quicker to get up and running, so go back and choose the documentation for that option unless you have clearly decided this is the option you want. The reason you might prefer to use the option described in this section, rather than the VirtualBox approach, is if you want improved performance for your :abbr:`IDE (integrated development environment)` and web browser.

Throughout this section you'll be typing commands into a terminal. To open a terminal (also called a shell) hold down the three keys :kbd:`Ctrl-Alt-t`. Make the terminal window as large as you need.

.. toctree::
    :maxdepth: 2
    :name: install-direct-toc

    install_direct_gitlab
    install_direct_git_repo_and_python
    install_direct_portal
    install_direct_development_env
