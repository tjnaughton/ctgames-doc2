============
Installation
============

.. toctree::
    :maxdepth: 1
    :name: toc-installation

    install_options
    install_zero
    install_virtualbox
    install_direct

.. history
.. authors
.. license
