.. _chap_template_game_explained:

.. centered:: Chapter: Template game explained

..
    **HIDDEN**
    .. contents:: On this page:


=================================
Introduction to the template game
=================================

The code in the CTGames new game template runs a very basic but functional game. This chapter helps you get started programming your own command-line CTGames game. It explains the purpose of each part of the automatically-generated code in a game newly-created from a template, and how it should be tailored for your new game idea.

.. seealso::

    Chapter ":ref:`Game development <chap_game_development>`" provides general guidelines for the development of CTGames games.

    Chapter ":ref:`Command-line games <chap_command_line_games>`" documents the programming interface for the CTGames framework with respect to command-line games.

    Chapter ":ref:`Web app games <chap_web_app_games>`" provides guidelines for how to turn a CTGames command-line game into a web app.


-----------------------------------------------------------
Explaining the code in a game newly-created from a template
-----------------------------------------------------------

The Python program :file:`create_new_game.py` (introduced in section ":ref:`sect_creating_a_new_game`") generates a new game from a template, which has very basic game functionality that can be described as follows. This game checks if the player can find the largest number in a list. It provides the player with a list of *n*-digit numbers and they should tell which is largest. For easier levels, *n* is smaller, and for harder levels *n* is larger. The value for *n* is not more than 6 for the hardest level.

This document explains the purpose of each part of the code in a game a newly-created from a template.

The document can also serve as a roadmap for a programmer new to the CTGames framework on what steps are needed to creating their own game given the starting point of a newly-created game from a template.

The files that can be modified are (only the first four need to be modified to make a command-line game):

    - :file:`logic.py` is where the game logic and all the data structures specific to your game are defined,
    - :file:`behaviour.py` is where the number of rounds in the game is defined and how the difficulty and game mechanics change from round to round,
    - :file:`text_constants.py` is where all of the static human-readable strings in the game are defined (e.g. question, instructions),
    - :file:`cl.py` is where the type and allowed values for the parameters that control game difficulty and game mechanics are defined, as well as any dynamic formatting of the human-readable strings in the game,
    - :file:`webapp/__init__.py` is where the code for the game's web app is defined, and
    - :file:`webapp/names.py` is where the graphical tutorial for your game is defined.


---------------------------
Play the newly-created game
---------------------------

The first step is to play the newly created game on the command-line to understand what this simple game is about so you'll better understand the explanations in this document. Assuming your newly-created game is in a directory called :file:`catchaball`, open a terminal in any directory and enter::

    conda activate ctgames
    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/catchaball/
    python cl.py

Play the game until you win. It should only take approx. three game rounds. Next, we'll specify specific parameters for a custom game round. In the same terminal session as used above, enter the following to understand how the two :py:class:`SublevelBehaviour` parameters affect the difficulty of each game round::

    python cl.py -c 5 3

After playing the game, enter the same line again. You will see that a different game instance was generated with the same game round parameters. This should help you understand the distinction between parameters to specify the difficulty of a game round (in this case ``5`` and ``3``), and multiple specific game instances generated from those parameters, two examples being ``[927, 975, 159, 583, 856]`` and ``[587, 585, 851, 236, 408]``.
