.. image:: ctgames_header.png

|

#####################################
Welcome to the CTGames documentation!
#####################################

If you are viewing these files locally, please note that the latest version is hosted on `ctgames.readthedocs.io <https://ctgames.readthedocs.io/>`_.

Please read the :ref:`overview section <sect_introduction_overview>` and the other sections in the introduction chapter if you are new to CTGames.

The tables of contents below and the sidebar on the left should let you easily access the documentation for your topic of interest. You can also use the search function in the top left corner.

The documentation is organised into chapters. Chapter ":ref:`Introduction <chap_introduction>`" explains what the project is, its overview and motivation, the technologies used, and the installation options. Chapter ":ref:`Game development <chap_game_development>`" gives general information for developers wishing to create games using the framework. Chapter ":ref:`Template game explained <chap_template_game_explained>`" explains how to get started programming a new command-line game with the CTGames framework. Chapter ":ref:`Command-line games <chap_command_line_games>`" documents the programming interface for the CTGames framework with respect to command-line games. Chapter ":ref:`Web app games <chap_web_app_games>`" provides guidelines for how to turn a CTGames command-line game into a web app. Chapter ":ref:`CTGames framework <chap_ctgames_framework>`" gives an overview of the framework and how games are executed within the framework. Chapter ":ref:`Teacher portal development <chap_portal_development>`" introduces the web app and back-end functionality that teachers use. Finally, chapter ":ref:`Game improvements <chap_game_improvements>`" catalogues work to be done on each of the games.

.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: Introduction
    :name: chap_introduction

    introduction/about
    introduction/overview
    introduction/technologies
    installation/index


.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: Game development

    game_development/introduction
    game_development/before_starting_game_development
    game_development/ctgames_code_style
    game_development/guidelines_interacting_repo
    game_development/creating_a_new_game
    game_development/resources


.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: Template game explained

    new_game_template/introduction
    new_game_template/game_behaviour
    new_game_template/command_line
    new_game_template/game_state
    new_game_template/game_logic
    new_game_template/string_constants


.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: Command-line games

    command_line_games/introduction
    command_line_games/guidelines_for_command_line_development
    command_line_games/function_create_game_round
    command_line_games/advanced_functionality_in_logic_py
    command_line_games/advanced_functionality_in_cl_py
    command_line_games/checklist_for_game_completion


.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: Web app games

    web_apps/introduction
    web_apps/guidelines_for_web_app_development
    web_apps/init_py
    web_apps/names_py
    web_apps/checklist_for_web_app_completion
    web_apps/textimageseq.py


.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: CTGames framework

    framework/introduction
    framework/separation_between_client_and_server


.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: Teacher portal development

    portal/introduction


.. toctree::
    :maxdepth: 1
    :numbered:
    :caption: Game improvements

    game_improvements/game_improvements


==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
