.. _sect_game_development_resources:

==========================
Game development resources
==========================

Various resources common to both command-line game development and web app game development will be collected here.

----------------------------------
Former students' final year theses
----------------------------------

At least twenty recent final year theses are available in the CTGames git repository. Ask your supervisor which would be the most recommended to read (by default, more recent theses might be more relevant). Please read a couple of them, to get an idea of the design decisions former CTGames developers made to create their games. You will also learn about the issues faced by former CTGames developers, and how they tackled them.


------
Videos
------

TODO


--------
Websites
--------

TODO
