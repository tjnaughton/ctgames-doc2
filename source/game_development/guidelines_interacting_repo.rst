.. _sect_guidelines_interacting_repo:

==========================================================
Guidelines for interacting with the CTGames git repository
==========================================================

This section is written for git novices that, outside of CTGames development, do not normally use git. For those familiar with git, this section can be summarised by the statement that developers of games in the CTGames framework are encouraged not to push development branches because their code is already separated in the filesystem from other games' (such developers can of course create any local development branches as they see fit, but are requested to merge into the `master` branch before pushing).


------------------------------
Restricted set of git commands
------------------------------

You have all been given powerful "Developer" privileges. You have the power to cause a lot of trouble for your fellow students by changing the git repository contents and/or history. Below are the only commands you should use unless you are a git expert. If you make a mistake using these commands -- no problem, these commands are safe so another CTGames developer will always be able to get you out of trouble. If you git commands that are not from the subsections below, you will be expected to fix all of your mistakes without me or your fellow students even finding out about it. Here is a cheat sheet of basic commands and usage from GitLab: https://about.gitlab.com/images/press/git-cheat-sheet.pdf, and this is a good website to learn about these commands: https://git-scm.com/docs (in particular, https://git-scm.com/docs/gittutorial).


Commands
^^^^^^^^

The only commands you actually need are::

    git status
    git pull
    git add my_file.py
    git add my_directory
    git rm
    git mv
    git commit
    git push

.. note:: When you run the ``git commit`` command, a text editor appears presenting you with a screen of information. Don't delete any existing text in this screen. Under the heading ``# Changes to be committed:`` this text lists what files you are about to commit. This gives you a valuable opportunity to double-check what you are committing, to ensure that you are not committing any files unrelated to your game, and that you are not committing changes to the CTGames framework unrelated to your own game. After double checking what files will be committed, type an informative one or two line commit message, press **Ctrl**-**x**, then **y** to accept, then press the **Enter** key.


Additional useful commands
^^^^^^^^^^^^^^^^^^^^^^^^^^

To graphically view the git repo history::

    gitk --all

To add all edits to previously added files in current directory and subdirectories::

    git add -u

To add and commit all changes you made (new and modified files)::

    git commit -a

To undo edits you made to a file since previous pull (important, if the file has been staged/"added" you have to undo that first and also note this command will delete your edits)::

    git checkout -- my_file.py

To undo an staging/"add" operation::

    git reset HEAD my_file.py

To undo the most recent add, but not committed, files/folders (note the full stop/period in this command)::

    git reset .

To undo a commit operation (important, the file should not have been pushed)::

    git reset --soft HEAD^

To undo a push operation::

    There is no satisfactory way to do this with an actively-developed shared repository, so please don't attempt it.


------------------
Guidelines for git
------------------

* You should ``git pull`` before commencing your code development each day you plan to work on the project, and in particular immediately before each commit operation. (This will avoid the majority of your potential headaches with git so please make sure to do it.)

* You should ``git commit`` your changes regularly, and at the very least at the end of each working day.

* You should ``git push`` immediately after each time you commit. Waiting offers no advantage, but does involve a risk that someone else might modify the same file(s) in the meantime.

* Do not edit files in directories outside the directories you create yourself, without discussing with me, as this might affect the running of other games.

* Because, as a game developer, you will restrict your development to your own directories, you can push to `master` (the default branch). By all means create local short-lived (e.g. alive for a day or two at most) branches for experimental development if you wish, however please do not push these branches to the remote repository. More details in the section :ref:`sect_development_branches`.


.. _sect_development_branches:

--------------------
Development branches
--------------------

All CTGames game developers push directly to the `master` branch. This not common among software development projects. The ability to create and push a development branch for one's contributions is one of the most useful features of git, and essential for collaborative development in most projects. Locally, CTGames game developers are encouraged to create development branches if they find them useful, but not to push them to the CTGames remote repository. The overhead is not necessary since each game's code is isolated from other games' code, and the CTGames framework is now quite stable.

Developers of the CTGames framework will push development branches, as their code may affect the running of other developer's games.
