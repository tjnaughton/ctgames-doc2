===========
ctgames-doc
===========

To read the contents of this repository, consider the version hosted at https://ctgames.readthedocs.io/ .

This repo contains the documentation for the CTGames framework for developing very simple computational thinking games (see `About <source/introduction/about.rst>`_ for more).
